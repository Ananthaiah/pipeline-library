// pipeline-library/vars/cPipeline.groovy

def call(String branchname, String appName) {
    pipeline {
        agent any
  
        environment {
            buildnumber = "${env.BUILD_NUMBER}"
         // Use the parameter, not the string literal
        }

        stages {
            stage('Git-clone') {
                steps {
                    echo "${branchname}" // Use double quotes to interpolate variables
                    echo "${appName}"    // Use double quotes to interpolate variables

                    catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                        git branch: "${branchname}", credentialsId: '717ebbb3-2d92-40fa-a279-0e1a032f3134', url: "https://gitlab.com/Ananthaiah/${appName}.git"
                        sh 'ls -la'
                    }
                }
            }

            stage('Build Docker Image') {
                steps {
                    sh "sudo docker build -t ${appName}-sit:${branchname}.v${buildnumber} ."
                }
            }

            stage('Push Docker Image to Amazon ECR') {
                steps {
                    script {
                        // Log in to Amazon ECR
                        sh "sudo aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 883135966974.dkr.ecr.us-east-1.amazonaws.com/${appName}-sit"
                        
                        // Tag Docker image
                        sh "sudo docker tag ${appName}-sit:${branchname}.v${buildnumber} 883135966974.dkr.ecr.us-east-1.amazonaws.com/${appName}-sit:${branchname}.v${buildnumber}"
                        
                        // Push Docker image
                        sh "sudo docker push 883135966974.dkr.ecr.us-east-1.amazonaws.com/${appName}-sit:${branchname}.v${buildnumber}"
                    }
                }
            }

            stage('Delete Docker Image in Jenkins Server') {
                steps {
                    catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                        // Delete Docker image
                        sh "sudo docker rmi ${appName}-sit:${branchname}.v${buildnumber} 883135966974.dkr.ecr.us-east-1.amazonaws.com/${appName}-sit:${branchname}.v${buildnumber}"
                    }
                }
            }
        }
    }
}
